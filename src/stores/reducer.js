
import {
    ADD_TO_CART,
    ADD_TO_FAVORITES,
    CLEAR_CART,
    CLOSE_MODAL,
    OPEN_MODAL,
    REMOVE_FROM_CART,
    REMOVE_FROM_FAVORITES,
    UPDATE_PRODUCT_LIST
} from "./actions"

const item = localStorage.getItem('Favorit Items');
const prodInCart = localStorage.getItem('Cart Items')

const defaultState = {
    products:[],
    favorites: item? JSON.parse(item) : [],
    cart: prodInCart? JSON.parse(prodInCart) :[],
    modal: false
}

function reducer(state = defaultState, action){

    switch(action.type){
        case UPDATE_PRODUCT_LIST:
            return {...state, products: action.payload}
        case ADD_TO_FAVORITES:
            return {...state, favorites: [...state.favorites, action.payload]}
        case REMOVE_FROM_FAVORITES:
            return {...state, favorites: [...state.favorites.filter(fav => action.payload.id !== fav.id)]}
        case ADD_TO_CART:
            return {...state, cart: [...state.cart, action.payload]}
        case REMOVE_FROM_CART:
            return {...state, cart: [...state.cart.filter(c => action.payload.id !== c.id)]}
        case OPEN_MODAL:
            return {...state, modal: action.payload}
        case CLOSE_MODAL:
            return {...state, modal: action.payload}
        case CLEAR_CART:
            return {...state, cart: []}

        default:
            return state
    }
}

export default reducer;