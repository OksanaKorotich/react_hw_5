
import './appStyles.scss';
import Header from "./components/Header/Header";
import Homepage from "./pages/Homepage/Homepage";
import { Route, Routes } from "react-router-dom";
import Cart from "./pages/Cart/Cart";
import Favorite from "./pages/Favorite/Favorite";


function App() {

  return (

    <div className="app">
      <Header />
      <Routes>
        <Route path="/" element={<Homepage />}/>
        <Route path="/cart" element={<Cart />}/>
        <Route path="/favorite" element={<Favorite />}/>
      </Routes>
    </div>
  );
}

export default App
