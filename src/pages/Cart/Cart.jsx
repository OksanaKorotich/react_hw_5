import { useSelector } from "react-redux";
import Modal from "../../components/Modal/Modal";
import Product from "./CartItems";
import styles from './cart.module.css'
import OrderForm from "../../components/formDelivery/formDelivery";



function Cart() {


    const cart = useSelector((state) => state.cart)

    // const cart =  JSON.parse(localStorage.getItem('Cart Items'))


    let sum = 0;
        cart.map(value => {
        sum += value['price'];
        });


    return (
        <>
        <h2 className={styles.title}>You have ${sum} worth of products in your cart</h2>
        <div className={styles.wrapper}>
            <div className={styles.products}>
                {cart.map(p =>
                <Product key = {p.id} info={p} />)}
            </div>

            <div className={styles.deliveryForm}>
                <h2 className={styles.form_title}>Order delivery</h2>
               <OrderForm />
            </div>

        </div>

              <Modal text = 'The product has been removed from the cart!' />
        </>
     );
}





export default Cart;