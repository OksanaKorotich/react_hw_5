import styles from './card.module.scss'
import Button from '../Button/Button'
import PropTypes from 'prop-types'
import {ReactComponent as Star} from '../../img/13487794321580594410.svg'
import { useDispatch, useSelector} from 'react-redux';
import { addToFavorites, addToCart, openModal, removeFromFavorites} from '../../stores/actions';
import { useEffect, useState } from 'react';




function Card({info}){

    const dispatch = useDispatch();
    const favorites = useSelector((state) => state.favorites)

    const addProductToFavorites = () => {
        dispatch(addToFavorites(info));
    }

    const removeProduct = () => {
        dispatch(removeFromFavorites(info));
    }

    const addProductToCart = () => {
        dispatch(addToCart(info))
    }

    const showModal = () =>{
        dispatch(openModal(true))
    }

    const [color, setColor] = useState()
    const {id:itemId} = info;

    useEffect(() =>{
        for (let i = 0; i <favorites.length; i++){
            if(favorites[i].id === itemId){
                setColor(true)
            }
        }
    } , [favorites, itemId])

    const changeColor = (color) =>{
        setColor(false)
    }

    function handler(){
        removeProduct();
        changeColor()
    }


     return(
        <div className={styles.cards}>
            <div className={styles.card__header}>
                <h1 className={styles.card__name}>{info.name}</h1>
                <button className={styles.carg__to_favorite} >
                    <Star className={color? styles.active: null} onClick={color? handler: addProductToFavorites}
                    />
                </button>
            </div>

            <img className={styles.card__img} src={info.url} alt="sofa" />
            <p className={styles.card__text}>Article: {info.article}</p>
            <p className={styles.card__text}>Color: {info.color} </p>
            <p className={styles.card__price}> Price: {info.price} $</p>
            <Button backgroundColor = 'gray' text = 'Add to cart'
            onClick={() => {addProductToCart(); showModal()}}
            />
        </div>
    )
}



Card.propTypes = {
    info: PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
        url: PropTypes.string.isRequired,
        article: PropTypes.number.isRequired,
        color: PropTypes.string.isRequired
    }).isRequired,

}

export default Card



