
import { useFormik } from 'formik';
import * as Yup from 'yup'
import styles from './formDelivery.module.css'
import {clearCart} from '../../stores/actions'
import { useDispatch } from 'react-redux';



function OrderForm(info) {

    const oderList = JSON.parse(localStorage.getItem('Cart Items'))
    const phoneNumber = /^(\+?\d{0,4})?\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{4}\)?)?$/

    const dispatch = useDispatch();
    const removeAll = () => {
        dispatch(clearCart(info));
    }


    const formik = useFormik({
        initialValues: {
            firstName: '',
            lastName:'',
            age: '',
            email:'',
            phone:'',
            address:''
        },
        validationSchema: Yup.object( {
            firstName: Yup.string()
            .max(15, 'Must be 15 characters or less!')
            .required('Required'),
            lastName: Yup.string()
            .max(20, 'Must be 20 characters or less!')
            .required('Required'),
            email: Yup.string()
            .email('Invalid email address!')
            .required('Required'),
            age: Yup.number()
            .max(100, 'Unreal age!')
            .required('Required'),
            phone: Yup.string()
            .matches(phoneNumber, 'Phone number is not valid')
            .min(10, "too short")
            .max(10, "too long")
            .required('Required'),
            address:Yup.string().required('Required')

        }),
        onSubmit: (values) => {
            console.log(oderList);
            console.log(values);
            localStorage.removeItem('Cart Items');
            removeAll()

        }
    });

    return (
        <form onSubmit={formik.handleSubmit}>
            <div className={styles.inputContainer}>
                <input
                    className={styles.field}
                    id='firstName'
                    name='firstName'
                    type="text"
                    placeholder='First Name'
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.firstName}/>
                    {formik.touched.firstName && formik.errors.firstName? <p className={styles.errors__msg}> {formik.errors.firstName}</p> : null }

                <input
                    className={styles.field}
                    id='lastName'
                    name='lastName'
                    type="text"
                    placeholder='Last Name'
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.lastName}/>
                    {formik.touched.lastName && formik.errors.lastName? <p className={styles.errors__msg}> {formik.errors.lastName}</p> : null }

                <input
                    className={styles.field}
                    id='age'
                    name='age'
                    type="text"
                    placeholder='Age'
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.age}/>
                    {formik.touched.age && formik.errors.age? <p className={styles.errors__msg}> {formik.errors.age}</p> : null }

                <input
                    className={styles.field}
                    id='email'
                    name='email'
                    type="email"
                    placeholder='Email'
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.email}/>
                    {formik.touched.email && formik.errors.email? <p className={styles.errors__msg}> {formik.errors.email}</p> : null }

                <input
                    className={styles.field}
                    id='phone'
                    name='phone'
                    type='phone'
                    placeholder='**********'
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.phone}/>
                    {formik.touched.phone && formik.errors.phone? <p className={styles.errors__msg}> {formik.errors.phone}</p> : null }

                <input
                    className={styles.field}
                    id='address'
                    name='address'
                    type="text"
                    placeholder='Address'
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.address}/>
                    {formik.touched.address && formik.errors.address? <p className={styles.errors__msg}> {formik.errors.address}</p> : null }
            </div>

            <button className={styles.form__btn} type='submit'>Checkout</button>


        </form>
     );
}

export default OrderForm;